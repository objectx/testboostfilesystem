#include "common.h"

//namespace fs = boost::filesystem ;
//namespace fs = std::tr2::sys ;

static const std::string unixify (const std::string &s) {
    std::string result ;
    std::transform (std::cbegin (s), std::cend (s), std::back_inserter (result), [](char ch) {
        if (ch == '\\') {
            return '/' ;
        }
        return ch ;
    }) ;
    return result ;
}

TEST_CASE ("Test <filesystem>", "[filesystem]") {
    const std::string s_path { "\\\\polyphony\\dfs\\programmer" } ;
    const std::string s_path_unix = unixify (s_path) ;

    SECTION ("Test with boost::filesystem") {
        boost::filesystem::path p { s_path } ;
        REQUIRE (p.string () == s_path) ;
        REQUIRE (p.root_directory () == "\\") ;
        REQUIRE (p.root_path ().string () == "\\\\polyphony\\") ;
        REQUIRE (p.root_name () == "\\\\polyphony") ;
        REQUIRE (p.stem () == "programmer") ;
    }
    SECTION ("Test with VC++'s <filesystem>") {
        std::tr2::sys::path p { s_path } ;
        REQUIRE (p.string () == s_path_unix) ;
        REQUIRE (p.root_directory () == "/") ;
        REQUIRE (p.root_path ().string () == "/") ;
        REQUIRE (p.root_name () == "") ;
        REQUIRE (p.stem () == "programmer") ;
    }
}
